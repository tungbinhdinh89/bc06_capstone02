export class CartItem1 {
  constructor(id, price, name, img, quantity) {
    this.id = id;
    this.price = price;
    this.name = name;
    this.img = img;
  }
}

export class CartItem2 {
  constructor(product, quantity) {
    this.product = product;
    this.quantity = quantity;
  }
}
