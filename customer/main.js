import { getListProduct, getProductByID } from './services/customer-service.js';
import {
  renderPhoneListProduct,
  renderShoppingCart,
  filter,
  onLoading,
  offLoading,
} from '../customer/controller/customer-controller.js';
import { CartItem1, CartItem2 } from './model/customer-model.js';

let cart = [];

// 4. function set data to localStorage
const CART = 'CART';

let dataJSON = localStorage.getItem(CART);
console.log('dataJSON: ', dataJSON);
if (dataJSON) {
  let dataRaw = JSON.parse(dataJSON);
  let ali = [];
  cart = dataRaw.map((item) => {
    return (ali = new CartItem2(item.product, item.quantity));
  });
  // console.log('item.product: ', item.product);
  // console.log('item.quantity: ', item.quantity);
  console.log('ali: ', ali);
}

let saveLocalStorage = () => {
  let dataJSON = JSON.stringify(cart);
  console.log('cart: ', cart);
  console.log('dataJSON: ', dataJSON);
  localStorage.setItem(CART, dataJSON);
};
// 5. function get data from localStorage

// show and hidden modal
window.openModal = () => {
  $('#exampleModal').modal('show');
  renderShoppingCart(cart);
};

window.closeModal = () => {
  return $('#exampleModal').modal('hide');
};

const BASE_URL = `https://640c9f16a3e07380e8f90b3d.mockapi.io`;
// show product
let fetchPhoneProduct = () => {
  onLoading();
  getListProduct()
    .then((res) => {
      console.log(res);
      renderPhoneListProduct(res.data);
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
fetchPhoneProduct();

// filter product
window.filterList = () => {
  getListProduct()
    .then((item) => {
      filter(item.data);
    })
    .catch((err) => {
      console.log('err: ', err);
    });
};

// add property CartItem1 at to product
let addCartItem = (itemIndex) => {
  let item = new CartItem1(
    itemIndex.id,
    itemIndex.price,
    itemIndex.name,
    itemIndex.img
  );
  console.log('itemTung: ', item);
  let quantity = 1;
  let product = new CartItem2(item, (quantity = 1));
  console.log('product: ', product);
  return product;
};

// add item to cart
window.addToCart = (id) => {
  getProductByID(id)
    .then((res) => {
      console.log(res);
      let newCart = addCartItem(res.data);
      console.log('newCart: ', newCart);
      let index = cart.findIndex((item) => {
        return item.product.id == newCart.product.id;
      });
      if (index == -1) {
        cart.push(newCart);
        console.log('cart: ', cart);
      } else {
        cart[index].quantity++;
      }
      swal('Added Success!', 'You clicked the button!', 'success');
      saveLocalStorage();
      renderShoppingCart(cart);
    })
    .catch((err) => {
      console.log(err);
      swal('Added Fail!', 'You clicked the button!', 'error');
    });
};

// increase item
window.increaseItemAmout = (id) => {
  cart[id].quantity++;
  renderShoppingCart(cart);
};

// increase item
window.decreaseItemAmount = (id) => {
  if (cart[id].quantity == 1) {
    cart.splice(id, 1);
    renderShoppingCart(cart);
  } else {
    cart[id].quantity--;
    renderShoppingCart(cart);
  }
};

// remove item in cart
window.removeItem = (id) => {
  cart.splice(id, 1);
  renderShoppingCart(cart);
};
window.removeItem = removeItem;

// empty cart
window.clearCart = () => {
  cart = [];
  closeModal();
  swal('Empty Cart!', 'You clicked the button!', 'warning');

  renderShoppingCart(cart);
};

//payment
window.payment = () => {
  cart = [];
  swal('Payment Success!', 'You clicked the button!', 'success');

  renderShoppingCart(cart);
};

// countineu shoping
window.countineuShoping = () => {
  closeModal();
};
