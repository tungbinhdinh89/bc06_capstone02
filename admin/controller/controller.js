// import { Phone } from '../model/model';

// render phone list

export let renderPhoneList = (phoneArr) => {
  let contentHTML = '';
  phoneArr.forEach((item) => {
    let contentTr = `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>
    ${currency(item.price, {
      symbol: 'đ',
    }).format()}
    </td>
    <td>${item.screen}</td>
    <td>${item.desc}</td>
    <td>
    <button class="btn btn-primary text-light " onClick='editPhone(${item.id})'>
    Edit   <i class="fa fa-edit"></i>
    </button>
    </td>
    <td><button class='btn btn-danger' onClick='deletePhone(${
      item.id
    })'>Delete  <i class="fa fa-trash"></i></button></td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById('tbodyPhone').innerHTML = contentHTML;
};

// push information to form
export let pushInformationToForm = (phoneList) => {
  document.getElementById('id').value = phoneList.id;
  document.getElementById('name').value = phoneList.name;
  document.getElementById('price').value = phoneList.price;
  document.getElementById('screen').value = phoneList.screen;
  document.getElementById('backCam').value = phoneList.backCamera;
  document.getElementById('frontCam').value = phoneList.frontCamera;
  document.getElementById('img').value = phoneList.img;
  document.getElementById('desc').value = phoneList.desc;
  document.getElementById('type').value = phoneList.type;
};

// show and hidden modal
export let showModal = () => {
  return $('#exampleModal').modal('show');
};

export let hideModal = () => {
  return $('#exampleModal').modal('hide');
};

// clear form
export let clearForm = () => {
  document.getElementById('formPhone').reset();
};

// get information from form

export let getInforFromForm = () => {
  let id = document.getElementById('id').value;
  let name = document.getElementById('name').value;
  let price = document.getElementById('price').value;
  let screen = document.getElementById('screen').value;
  let backCamera = document.getElementById('backCam').value;
  let frontCamera = document.getElementById('frontCam').value;
  let img = document.getElementById('img').value;
  let desc = document.getElementById('desc').value;
  let type = document.getElementById('type').value;
  return {
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};

// show  status message

export let showMessage = (mesage) => {
  Toastify({
    text: mesage,
    duration: 3000,
    destination: 'https://github.com/apvarun/toastify-js',
    newWindow: true,
    close: true,
    gravity: 'top', // `top` or `bottom`
    position: 'right', // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: 'linear-gradient(to right, #00b09b, #96c93d)',
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

// on loading
export let onLoading = () => {
  document.querySelector('.loading').style.display = 'flex';
};

// off loading
export let offLoading = () => {
  document.querySelector('.loading').style.display = 'none';
};

// hidden and show button
export let hiddenButton = (key1, key2, key3) => {
  document.querySelector('.editInfor').innerHTML = key1;
  document.getElementById('btnCapNhat').style.display = key2;
  document.getElementById('btnThemMon').style.display = key3;
};
