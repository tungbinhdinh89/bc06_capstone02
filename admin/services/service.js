// import { Phone } from '../model/model';
import { renderPhoneList } from '../controller/controller.js';
import { pushInformationToForm } from '../controller/controller.js';
import { getInforFromForm } from '../controller/controller.js';
import { showModal } from '../controller/controller.js';
import { hideModal } from '../controller/controller.js';
import { clearForm } from '../controller/controller.js';
import { showMessage } from '../controller/controller.js';
import { hiddenButton } from '../controller/controller.js';
import { onLoading } from '../controller/controller.js';
import { offLoading } from '../controller/controller.js';

const BASE_URL = `https://640c9f16a3e07380e8f90b3d.mockapi.io`;

let fetchPhoneList = () => {
  onLoading();

  axios({
    url: `${BASE_URL}/phone`,
    method: `get`,
  })
    .then((res) => {
      console.log(res);
      renderPhoneList(res.data);
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
fetchPhoneList();

// delete

window.deletePhone = (id) => {
  onLoading();
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: `delete`,
  })
    .then((res) => {
      console.log(res);
      fetchPhoneList();
      showMessage('Delete Phone Success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

// edit
window.editPhone = (id) => {
  onLoading();
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: `get`,
  })
    .then((res) => {
      console.log(res);
      hiddenButton('Edit Phone', 'flex', 'none');
      pushInformationToForm(res.data);
      showModal();
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

// update
window.updatePhone = () => {
  onLoading();
  let phoneList = getInforFromForm();
  axios({
    url: `${BASE_URL}/phone/${phoneList.id}`,
    method: `put`,
    data: getInforFromForm(),
  })
    .then((res) => {
      console.log(res);
      fetchPhoneList();
      showMessage('Update Phone Success');
      clearForm();
      hideModal();
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

// add
window.addPhone = () => {
  onLoading();
  axios({
    url: `${BASE_URL}/phone`,
    method: `post`,
    data: getInforFromForm(),
  })
    .then((res) => {
      console.log(res);
      console.log('getInforFromForm(): ', getInforFromForm());
      fetchPhoneList();
      showMessage('Add Phone Success');
      clearForm();
      hideModal();
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

window.editInfor = () => {
  hiddenButton('Add New Phone', 'none', 'flex');
};
