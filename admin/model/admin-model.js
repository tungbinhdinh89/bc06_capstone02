export class Product {
  constructor(
    id,
    name,
    price,
    screen,
    blackCamera,
    frontCamera,
    img,
    phoneDescription,
    phoneBrand
  ) {
    (this.id = id),
      (this.name = name),
      (this.price = price),
      (this.screen = screen),
      (this.blackCamera = blackCamera),
      (this.frontCamera = frontCamera),
      (this.img = img),
      (this.phoneDescription = phoneDescription),
      (this.phoneBrand = phoneBrand);
  }
}
